const mongoose = require('mongoose');

const ToDoSchema = new mongoose.Schema({
    id: String,
    title: String,
    description: String

});
const ToDo = mongoose.model('ToDo', ToDoSchema);
module.exports = ToDo

