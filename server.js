var express = require('express');
var bodyParser = require('body-parser');
var app     = express();
const Book = require('./models/BookModel');
const ToDo = require('./models/toDoModel');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function(){
});


app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

app.get('/book', (req, res) => {
    const book = new Book({title:'El señor de los anillos'});
    book.save((err, storedBook) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(storedBook)
    });
});

app.get('/books', (req, res) => {
    Book.find({},(err, books) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(books)
    });
    //Item.find({name: 'Quijote'}, callback);
});


app.get('/findBook', (req, res) => {
    Book.findById("5cd4072144edc85158673f73", function (err, book) {
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        book.title = "TitleNew";
        book.save((err, storedBook) =>{
            if (err) {
                return res.status(500).send({"error":"fallo"})
            }
            return res.json(storedBook)
        });
    });
});




app.get('/toDoElement', (req, res) => {
    ToDo.find({},(err, toDoList) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(toDoList)
    });
    //Item.find({name: 'Quijote'}, callback);
});

app.post('/toDoElement', (req, res) => {
    const toDo = new ToDo(req.body);
    toDo.save((err, storedToDo) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(storedToDo)
    });
});

app.delete('/toDoElement/:id', (req, res) => {
    ToDo.findOneAndRemove( { "_id" : req.params.id }, (err) => {
        console.log("Borrando: " + req.params.id)
        if (err) {
            return console.error(err);
        }
        console.log("deleting")
    });
});



app.put('/todoElement/:id',function(req,res){
    ToDo.findById(req.params.id, function (err, toDoItem) {
        console.log(req.body)
        toDoItem.title = req.body.title
        toDoItem.description = req.body.description
        toDoItem.save((err, storedItem) =>{
            if (err) {
                return res.status(500).send({"error":"fallo"})
            }
            return res.json(storedItem)
        });
    });
});







app.listen(3000);
